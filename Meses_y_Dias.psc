Algoritmo Meses_y_Dias
	Definir D Como numerica
	Definir M Como numerica
	Escribir ""
	
	Escribir "Digite Mes De 1 Al 12"
	Leer M
	

	
	Segun M Hacer
		1:
			Escribir "El Mes Es Enero"
		2:
			Escribir "El Mes Es Febrero"
		3:
			Escribir "El Mes Es Marzo"
		4:
			Escribir "El Mes Es Abril"
		5:
			Escribir "El Mes Es Mayo"
		6:
			Escribir "El Mes Es Junio"
		7:
			Escribir "El Mes Es Julio"
		8:
			Escribir "El Mes Es Agosto"
		9:
			Escribir "El Mes Es Septiembre"
		10:
			Escribir "El Mes Es Octubre"
		11:
			Escribir "El Mes Es Noviembre"
		12:
			Escribir "El Mes Es Diciembre"
			
			
		De Otro Modo:
			Escribir "Ese Mes No Existe";
			
			Escribir ""
	Fin Segun
	
	Escribir ""
	Escribir "Digite Dia De 1 Al 7"
	Leer D
	
	
	Segun D Hacer
		1:
			Escribir "El Dia Es Lunes"
		2:
			Escribir "El Dia Es Martes"
		3:
			Escribir "El Dia Es Miercoles"
		4:
			Escribir "El Dia Es Jueves"
		5:
			Escribir "El Dia Es Viernes"
		6:
			Escribir "El Dia Es Sabado"
		7:
			Escribir "El Dia Es Domingo"
			
		De Otro Modo:
			Escribir "Ese Dia No Existe";
			
	Fin Segun
	
FinAlgoritmo
